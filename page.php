<?php get_header(); ?>

		<div class="col-sm-9 col-sm-push-3 col-xs-12">
			<?php while (have_posts()) : the_post(); ?>
					<?php if (!get_field('hide_page_title', $post->ID)): ?>
                      <h1><?php the_title(); ?></h1>
                    <?php endif; ?>
                         <div class="content">
                            <?php the_content(); ?>
                        </div>
           <?php endwhile; ?>
        </div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>