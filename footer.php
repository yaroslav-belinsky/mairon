<?php  $options = get_option('theme_options_panel');  ?>
    </div>
</div>
<?php if(is_front_page()){ ?>
<div class="container-fluid map-container">
  <div class="row">
      <script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=IB5U9gSbg2RY6bZBSmOFKsxZo0G2sS_0&width=100%&height=280&lang=ru_UA&sourceType=constructor"></script>
      <div class="col-md-10 col-md-offset-1 contacts-on-map">
        <div class="row">
          <div class="col-md-4">
            <div class="row">
              <div class="col-md-12 contacts-on-map-adress-title">МЫ НАХОДИМСЯ</div>
              <div class="col-md-12 contacts-on-map-adress"><?php echo $options['footer_adress']; ?></div>
            </div>
          </div>
          <div class="col-md-6 contacts-on-map-center">
            <div class="row">
              <div class="col-md-6 contacts-on-map-phone"><?php echo $options['footer_phone']; ?></div>
              <div class="col-md-6"><button class="button-call-back" data-toggle="modal" data-target="#modalOrder"> </button></div>
            </div>
          </div>
          <div class="col-md-2">
            <div class="row">
              <div class="col-md-12 text-center">Узнать больше о нас:</div>
              <div class="col-md-12 text-center">
                <?php foreach ($options['re_socials_footer'] as $key => $social) { ?>
                  <?php if ($social['re_socials_footer_active']) :?>
                     <a href="<?php echo $social['re_socials_footer_link']; ?>" class="social-item"><img src="<?php echo $social['re_socials_footer_img']['src']; ?>" class="img-responsive" alt="Responsive image"></a>
                  <?php  endif; ?>
               <?php  } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>
<?php } ?>
<div class="container-fluid footerblock">
  <div class="container">
  <div class="row">
  <div class="col-md-10 footernavigation">
    <ul class="nav nav-pills">
                            <?php
                            $menu_name = 'footer';
                            if (($locations = get_nav_menu_locations()) && isset($locations[$menu_name])) {
                                $menu = wp_get_nav_menu_object($locations[$menu_name]);
                                $menu_items = wp_get_nav_menu_items($menu->term_id);
                                $thePostID = $post->ID;
                                foreach ((array)$menu_items as $key => $menu_item) {
                                    $title = $menu_item->title;
                                    $url = $menu_item->url;
                                    if ($thePostID == $menu_item->object_id) {
                                        echo '<li role="presentation" class="active"><a href="' . $url . '">' . $title . '</a></li>';
                                    } else {
                                        echo '<li role="presentation"><a href="' . $url . '">' . $title . '</a></li>';
                                    }
                                }
                            }
                            ?>

     
    </ul>
    <div class="row footer-contacts">
      <div class="row footer_adress">
        <div class="col-sm-5">
          <div class="col-sm-1">
            <img src="<?php echo get_template_directory_uri();?>/images/map.png">
          </div>
          <div class="col-sm-11">
            <?php echo $options['footer_adress']; ?>
          </div>
        </div>
      </div>
      <div class="row footer_email">
        <div class="col-sm-5">
          <div class="col-sm-1">
            <img src="<?php echo get_template_directory_uri();?>/images/mail.png">
          </div>
          <div class="col-sm-11">
            <?php echo $options['footer_email']; ?>
          </div>
        </div>
      </div>
      <div class="row footer_phone">
        <div class="col-sm-5">
          <div class="col-sm-1">
            <img src="<?php echo get_template_directory_uri();?>/images/phone.png">
          </div>
          <div class="col-sm-11">
            <?php echo $options['footer_phone']; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-2 right-footer">
    <div class="row">
    <div class="col-md-12 text-center ">
      <div class="opacity-65">Создание сайта:</div>
      <img src="<?php echo get_template_directory_uri (); ?>/images/kit_logo.png">
    </div>
    <div class="col-sm-12 footer-search"><a href="/search"><img src="<?php echo get_template_directory_uri(); ?>/images/search.png"></a></div>
    <div class="col-sm-12 footer_copyright"><?php echo $options['footer_copyright']; ?></div>
  </div>
  </div>
</div>
</div>
</div>
<!-- Modal order -->
<div class="modal fade" id="modalOrder" tabindex="-1" role="dialog" aria-labelledby="modalOrderLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
        <h4 class="modal-title" id="modalOrderLabel">Заказать звонок</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
              <?php echo do_shortcode('[contact-form-7 id="82" title="Контактная форма 1"]'); ?>
            </div>
    </div>
    </div>
      </div>
    </div>
  </div>
</div>
          
            <?php wp_footer(); ?>
</body>

</html>

