<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="http://mairon-studio.ru">
    <?php wp_head(); ?>
    <title><?php wp_title(''); ?><?php if (wp_title('', false)) {
            echo ' | ';
        } ?><?php bloginfo('name'); ?></title>
  
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<?php  $options = get_option('theme_options_panel');  ?>
<?php //echo "<pre>"; var_dump($options); echo"</pre>";?>
<body <?php body_class(); ?>>
<div class="container-fluid headerblock">
  <div class="container headercontainer">
    <div class="row">
      <div class="col-md-7"><a href="/"><img src="<?php echo $options['header_logo']['src']; ?>" class="img-responsive" alt="Responsive image"></a></div>
      <div class="col-md-3">
         <div class="row phone">
          <div class="col-md-12 header_phone text-right"><?php echo $options['header_phone']; ?></div>
          <div class="col-md-12 text-center"><button class="button-call-back" data-toggle="modal" data-target="#modalOrder"> </button></div>
        </div>
      </div>
      <div class="col-md-2 text-right">
        <a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>">
            <img src="<?php echo get_template_directory_uri(); ?>/images/cart_icon.png">
            <div class="count"><?php echo sprintf (_n( '%d товар', '%d товаров', WC()->cart->cart_contents_count ), WC()->cart->cart_contents_count ); ?></div>
            <div class="cost"><?php echo WC()->cart->get_cart_total(); ?></div>
        </a>
      </div>
    </div>
    <?php
      if(is_front_page()) {
        echo do_shortcode('[metaslider id=52]');
      }
     ?>
   
    <nav class="navbar navbar-default">
                <div class="container-fluid nopadding">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand visible-xs" href="/">
                            Site name
                        </a>
                    </div>
                    <div class="collapse navbar-collapse nopadding" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <?php
                            $menu_name = 'primary';
                            if (($locations = get_nav_menu_locations()) && isset($locations[$menu_name])) {
                                $menu = wp_get_nav_menu_object($locations[$menu_name]);
                                $menu_items = wp_get_nav_menu_items($menu->term_id);
                                $thePostID = $post->ID;
                                foreach ((array)$menu_items as $key => $menu_item) {
                                    $title = $menu_item->title;
                                    $url = $menu_item->url;
                                    if ($thePostID == $menu_item->object_id) {
                                        echo '<li class="active"><a href="' . $url . '">' . $title . '</a></li>';
                                    } else {
                                        echo '<li><a href="' . $url . '">' . $title . '</a></li>';
                                    }
                                }
                            }
                            ?>

                        </ul>
                      
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>
            <div class="row search-row">
                <div class="col-md-8">
                    <?php get_product_search_form( ); ?>
                </div>
                <div class="col-md-4">
                    <a href="#" class="actions">Акции</a>
                    <a href="#" class="new-products">Новинки</a>
                </div>
            </div>
  </div>

</div>
<div class="container" id="content_part">
    <div class="row">