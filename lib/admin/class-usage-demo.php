<?php
/*
Plugin Name: Demo Admin Page
Plugin URI: http://en.bainternet.info
Description: My Admin Page Class usage demo
Version: 1.2.9
Author: Bainternet, Ohad Raz
Author URI: http://en.bainternet.info
*/



  //include the main class file
  require_once("admin-page-class/admin-page-class.php");
  
  
  /**
   * configure your admin page
   */
  $config = array(    
    'menu'           => array(),             //sub page to settings page
    'icon_url'   => get_stylesheet_directory_uri() . '/images/mairon-logo-small.png',
    'page_title'     => __('Настройки темы','apc'),       //The name of this page 
    'capability'     => 'edit_themes',         // The capability needed to view the page 
    'option_group'   => 'theme_options_panel',       //the name of the option to create in the database
    'id'             => 'admin_page',            // meta box id, unique per page
    'fields'         => array(),            // list of fields (can be added by field arrays)
    'local_images'   => false,          // Use local or hosted images (meta box images for add/remove)
    'use_with_theme' => get_stylesheet_directory_uri() . '/lib/admin/admin-page-class/'          //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
  );  
  
  /**
   * instantiate your admin page
   */
  $options_panel = new BF_Admin_Page_Class($config);
  $options_panel->OpenTabs_container('');
  
  /**
   * define your admin page tabs listing
   */
  $options_panel->TabsListing(array(
    'links' => array(
      'options_1' =>  __('Header','apc'),
      'options_2' =>  __('Footer','apc'),
      'options_3' =>  __('Sidebar','apc'),
    )
  ));
  
  /**
   * Open admin page first tab
   */
  $options_panel->OpenTab('options_1');

  /**
   * Add fields to your admin page first tab
   * 
   * Simple options:
   * input text, checbox, select, radio 
   * textarea
   */
  //title
  $options_panel->Title(__("Header","apc"));

  $options_panel->addImage('header_logo',array('name'=> __('Логотип ','apc'),'preview_height' => '120px', 'preview_width' => '440px'));
  
  $options_panel->addText('header_phone', array('name'=> __('Телефон','apc')));

 
  $options_panel->CloseTab();


  /**
   * Open admin page Second tab
   */
  $options_panel->OpenTab('options_2');
  /**
   * Add fields to your admin page 2nd tab
   * 
   * Fancy options:
   *  typography field
   *  image uploader
   *  Pluploader
   *  date picker
   *  time picker
   *  color picker
   */
  //title
  $options_panel->Title(__('Footer','apc'));

  
  $options_panel->addTextarea('footer_adress', array('name'=> __('Адрес ','apc')));

  $options_panel->addText('footer_email', array('name'=> __('Email ','apc')));
  
  $options_panel->addText('footer_phone', array('name'=> __('Телефон ','apc')));

  $repeater_fields[] = $options_panel->addTextarea('re_socials_footer_link',array('name'=> __('Ссылка ','apc')),true);
  $repeater_fields[] = $options_panel->addImage('re_socials_footer_img',array('name'=> __('Иконка ','apc')),true);
  $repeater_fields[] = $options_panel->addCheckbox('re_socials_footer_active',array('name'=> __('Активность  ','apc')),true);
  
  /*
   * Then just add the fields to the repeater block
   */
  //repeater block
  $options_panel->addRepeaterBlock('re_socials_footer',array('sortable' => true, 'inline' => true, 'name' => __('Социальные сети','apc'),'fields' => $repeater_fields, 'desc' => __('Блок социальных сетей','apc')));
  
  $options_panel->addTextarea('footer_copyright',array('name'=> __('Информационный блок в футере ','apc')));
  //$options_panel->addWysiwyg('footer_copyright',array('name'=> __('Информационный блок в футере ','apc')));
  /**
   * Close second tab
   */ 
  $options_panel->CloseTab();

  $options_panel->OpenTab('options_3');
  $options_panel->Title(__('Sidebar','apc'));

  $re_partners[] = $options_panel->addTextarea('re_partners_link',array('name'=> __('Ссылка ','apc')),true);
  $re_partners[] = $options_panel->addImage('re_partners_img',array('name'=> __('Иконка ','apc')),true);
  $re_partners[] = $options_panel->addCheckbox('re_partners_active',array('name'=> __('Активность  ','apc')),true);
  
  /*
   * Then just add the fields to the repeater block
   */
  //repeater block
  $options_panel->addRepeaterBlock('re_partners',array('sortable' => true, 'inline' => true, 'name' => __('Партнеры','apc'),'fields' => $re_partners, 'desc' => __('Блок партнеров','apc')));


  $re_actions[] = $options_panel->addTextarea('re_actions_link',array('name'=> __('Ссылка ','apc')),true);
  $re_actions[] = $options_panel->addImage('re_actions_img',array('name'=> __('Иконка ','apc')),true);
  $re_actions[] = $options_panel->addCheckbox('re_actions_active',array('name'=> __('Активность  ','apc')),true);
  
  /*
   * Then just add the fields to the repeater block
   */
  //repeater block
  $options_panel->addRepeaterBlock('re_actions',array('sortable' => true, 'inline' => true, 'name' => __('Реклама','apc'),'fields' => $re_actions, 'desc' => __('Блок рекламы','apc')));

  $options_panel->CloseTab();

  $options_panel->CloseTab();

  //Now Just for the fun I'll add Help tabs
  $options_panel->HelpTab(array(
    'id'      =>'tab_id',
    'title'   => __('My help tab title','apc'),
    'content' =>'<p>'.__('This is my Help Tab content','apc').'</p>'
  ));
  $options_panel->HelpTab(array(
    'id'       => 'tab_id2',
    'title'    => __('My 2nd help tab title','apc'),
    'callback' => 'help_tab_callback_demo'
  ));
  
  //help tab callback function
  function help_tab_callback_demo(){
    echo '<p>'.__('This is my 2nd Help Tab content from a callback function','apc').'</p>';
  }