<?php 

function product_query_mairon( $atts ) {
    extract( shortcode_atts( array(
        'category' => '1',
        'posts_per_page' => 4,
    ), $atts ) );
        
        $args = array(
            'post_type' => 'product',
            'posts_per_page' => $posts_per_page,
            'orderby' => 'date',
            'order' => 'ASC',
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'product_cat',
                    'field' => 'id',
                    'terms' => $category
                )
            ));
        $loop = new WP_Query( $args );
        if ( $loop->have_posts() ) {
            echo '<div class="row">';
            while ( $loop->have_posts() ) : $loop->the_post();
                wc_get_template_part( 'content', 'product' );
            endwhile;
            echo '</div>';
            echo '<div class="row view-all-row"><div><a href="#">Смотреть все модели</a></div></div>';
        } else {
            echo __( 'No products found' );
        }
        wp_reset_postdata();
   
}
add_shortcode( 'product_query_mairon', 'product_query_mairon' );

function product_cat_query_mairon( $atts ) {
    extract( shortcode_atts( array(
        'exclude' => '6, 10',
    ), $atts ) );
        
              $taxonomy     = 'product_cat';
              $orderby      = 'name';  
              $show_count   = 0;      // 1 for yes, 0 for no
              $pad_counts   = 0;      // 1 for yes, 0 for no
              $hierarchical = 1;      // 1 for yes, 0 for no  
              $title        = '';  
              $empty        = 0;

              $args = array(
                     'taxonomy'     => $taxonomy,
                     'orderby'      => $orderby,
                     'show_count'   => $show_count,
                     'pad_counts'   => $pad_counts,
                     'hierarchical' => $hierarchical,
                     'title_li'     => $title,
                     'hide_empty'   => $empty,
                     'exclude' => $exclude
              );
              echo '<ul class="sidebar-menu">';
             $all_categories = get_categories( $args );
             foreach ($all_categories as $cat) {
                if($cat->category_parent == 0) {
                    $category_id = $cat->term_id;       
                    echo '<li class="parent-menu-item"><a href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a>';

                    $args2 = array(
                            'taxonomy'     => $taxonomy,
                            'child_of'     => 0,
                            'parent'       => $category_id,
                            'orderby'      => $orderby,
                            'show_count'   => $show_count,
                            'pad_counts'   => $pad_counts,
                            'hierarchical' => $hierarchical,
                            'title_li'     => $title,
                            'hide_empty'   => $empty
                    );
                    $sub_cats = get_categories( $args2 );
                    if($sub_cats) {
                        echo '<ul class="sub-menu">';
                          foreach($sub_cats as $sub_category) {
                              echo  '<li class="sub-menu-item"><a href="'.get_term_link($sub_category->slug, 'product_cat').'">'.$sub_category->name.'</a></li>' ;
                          }   
                        echo '</ul>';
                    }
                    echo '</li>';
                }       
            }
            echo '</ul>';
        wp_reset_postdata();
   
}
add_shortcode( 'product_cat_query_mairon', 'product_cat_query_mairon' );
//Products wrapper
/*remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);

function product_loop_wrapper_mairon () {
	echo '<div class="row">';
}
add_action('woocommerce_before_main_content','product_loop_wrapper_mairon',10);
*/
/*add_filter( 'post_class', function($classes){
    global $post;
    if ($post->post_type == 'product' && !is_product()){
        $classes[] = 'col-md-3 col-sm-6 col-xs-12';
    }
    return $classes;
});*/

//
function woocommerce_template_loop_product_excerpt () {
	global $post;
	echo '<div class="product_excerpt">'.$post->post_excerpt.'</div>';
}
add_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_excerpt', 20);

//Remove add to cart
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);

//Wrapped img product
remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
add_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail_wrapper', 10);

function woocommerce_template_loop_product_thumbnail_wrapper () {
    echo '<div class="product-img-wrapper">';
    echo woocommerce_get_product_thumbnail();
    echo '</div>';

}

//Add product quntity to loop
add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_quntity',20);
function woocommerce_template_loop_quntity () {

}

//Archive wrapper
/*remove_action('woocommerce_before_main_content','woocommerce_output_content_wrapper',10);
add_action('woocommerce_before_main_content','woocommerce_output_content_wrapper_mairon',10);
function woocommerce_output_content_wrapper_mairon () {
  echo '<div class="col-sm-9 col-sm-push-3 col-xs-12 page-content-container">';
}
remove_action('woocommerce_after_main_content','woocommerce_output_content_wrapper_end',10);
add_action('woocommerce_sidebar','woocommerce_after_main_content',5);
function woocommerce_output_content_wrapper_end_mairon () {
  echo '</div>';
}*/

//Sidebar replace
//remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar',10);
//add_action('woocommerce_after_main_content', 'woocommerce_get_sidebar', 5);

//
add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_remains', 20);
function woocommerce_template_loop_remains () {
  global $product;
   $availability      = $product->get_total_stock();
    
    echo "<div class='product_remains'>Осталось ".$availability. " шт.</div>";
}


remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating',10);
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating',5);


remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title',5);
add_action('woocommerce_before_single_product_summary', 'woocommerce_template_single_title', 5);

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta',40);


add_action('woocommerce_single_product_summary', 'woocommerce_template_single_desc', 5);
function woocommerce_template_single_desc () {
  global $product;
  echo '<div class="single-product-desc">';
  echo $product->post->post_content;
    $has_row    = false;
    $alt        = 1;
    $attributes = $product->get_attributes();

    ob_start();

    ?>
   
    <table class="shop_attributes">

      <?php if ( $product->enable_dimensions_display() ) : ?>

        <?php if ( $product->has_weight() ) : $has_row = true; ?>
          <tr class="<?php if ( ( $alt = $alt * -1 ) == 1 ) echo 'alt'; ?>">
            <th><?php _e( 'Weight', 'woocommerce' ) ?></th>
            <td class="product_weight"><?php echo $product->get_weight() . ' ' . esc_attr( get_option( 'woocommerce_weight_unit' ) ); ?></td>
          </tr>
        <?php endif; ?>

        <?php if ( $product->has_dimensions() ) : $has_row = true; ?>
          <tr class="<?php if ( ( $alt = $alt * -1 ) == 1 ) echo 'alt'; ?>">
            <th><?php _e( 'Dimensions', 'woocommerce' ) ?></th>
            <td class="product_dimensions"><?php echo $product->get_dimensions(); ?></td>
          </tr>
        <?php endif; ?>

      <?php endif; ?>

      <?php foreach ( $attributes as $attribute ) :
        if ( empty( $attribute['is_visible'] ) || ( $attribute['is_taxonomy'] && ! taxonomy_exists( $attribute['name'] ) ) ) {
          continue;
        } else {
          $has_row = true;
        }
        ?>
        <tr class="<?php if ( ( $alt = $alt * -1 ) == 1 ) echo 'alt'; ?>">
          <th><?php echo wc_attribute_label( $attribute['name'] ); ?></th>
          <td><?php
            if ( $attribute['is_taxonomy'] ) {

              $values = wc_get_product_terms( $product->id, $attribute['name'], array( 'fields' => 'names' ) );
              echo apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values );

            } else {

              // Convert pipes to commas and display values
              $values = array_map( 'trim', explode( WC_DELIMITER, $attribute['value'] ) );
              echo apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values );

            }
          ?></td>
        </tr>
      <?php endforeach; ?>

    </table>
    <?php
    if ( $has_row ) {
      echo ob_get_clean();
    } else {
      ob_end_clean();
    }
    $availability      = $product->get_total_stock();
    
    echo "Осталось ".$availability. " шт.";
    echo '</div>';

    

}
?>
