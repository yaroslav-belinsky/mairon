<?php

add_theme_support('post-thumbnails');

//set_post_thumbnail_size(160, 160, false);
//add_image_size( 'service-thumb', 300, 200, true );

remove_filter('the_content', 'wptexturize');
function theme_setup () {

    register_nav_menus( array(

        'primary'   => __( 'Верхнее меню', 'mairon' ),

        'footer' => __( 'Футтер меню', 'mairon' ),
        
        'cartmenu' => __( 'Меню для корзины', 'mairon' ),

    ) );

    


}

function enqueue_scripts () {
    // cache the directory path, maybe helpful?

        $template_directory = get_template_directory_uri();

        // all styles

        wp_enqueue_style('bootstrap', $template_directory . '/css/bootstrap.css');
        wp_enqueue_style('owl-carousel', $template_directory . '/lib/owl-carousel/owl.carousel.css');
        wp_enqueue_style('owl-theme', $template_directory . '/lib/owl-carousel/owl.theme.css');
        wp_enqueue_style('owl-transitions', $template_directory . '/lib/owl-carousel/owl.transitions.css');
        wp_enqueue_style('jquery-fancybox', $template_directory . '/lib/fancybox/jquery.fancybox.css');
        wp_enqueue_style('style', $template_directory . '/style.css');

        // all scripts

        wp_enqueue_script('owl-carousel', $template_directory . '/lib/owl-carousel/owl.carousel.min.js', array(
            'jquery'
        ) , '20120206', true);
        /*Fancybox*/
         wp_enqueue_script('fancybox', $template_directory . '/lib/fancybox/jquery.fancybox.js', array(
            'jquery'
        ) , '20120206', true);
         wp_enqueue_script('fancybox', $template_directory . '/lib/fancybox/helpers/jquery.fancybox-media.js', array(
            'jquery'
        ) , '20120206', true);
        wp_enqueue_script('fancybox-media', $template_directory . '/lib/fancybox/helpers/jquery.fancybox-media.js', array(
            'jquery',
            'fancybox'
        ) , '20120206', true);

        wp_enqueue_script('bootstrap', $template_directory . '/js/bootstrap.min.js', array(
            'jquery'
        ) , '20120206', true);
        wp_enqueue_script('scripts', $template_directory . '/js/scripts.js', array(
            'jquery',
            'bootstrap',
            'owl-carousel'
        ) , '20120206', true);
}

add_action( 'after_setup_theme', 'theme_setup' );

add_action('wp_enqueue_scripts', 'enqueue_scripts');

function posts_slider_mairon( ) {

        $args = array(
            'post_type' => 'post',
            'posts_per_page' => 9999999,
            'orderby' => 'date',
            'order' => 'DESC'
            );
        $loop = new WP_Query( $args );
        if ( $loop->have_posts() ) {
            echo '<div class="owl-carousel posts-slider">';
            $count_posts = 1;
            while ( $loop->have_posts() ) : $loop->the_post();
                if($count_posts%2 !== 0) {
                    echo '<div class="item">';
                }

                    echo '<div class="post-slider-item row">';
                        echo '<div class="col-sm-4 post-slider-img">';
                            the_post_thumbnail();
                        echo '</div>';
                        echo '<div class="col-sm-8">';
                            echo '<div class="row">';
                                echo '<div class="col-sm-12 post-slider-date">';
                                    echo '<img class="post-slider-date-celendar" src="'.get_template_directory_uri().'/images/celendar.png">';
                                    echo get_the_date('d / m / Y');
                                echo '</div>';
                                echo '<div class="col-sm-12 post-slider-title">';
                                    echo '<a href="'.get_the_permalink().'">';
                                     the_title();
                                    echo '</a>';
                                echo '</div>';
                                echo '<div class="col-sm-12 post-slider-content">';
                                    the_content();
                                echo '</div>';
                            echo '</div>';
                        echo '</div>';
                    echo '</div>';

                if($count_posts%2 == 0 || $count_posts == count($loop->posts))  {
                    echo '</div> ';
                }
                $count_posts++;
            endwhile;
            echo '</div>';
        } else {
            echo __( 'No products found' );
        }
        wp_reset_postdata();
   
}
add_shortcode( 'posts_slider_mairon', 'posts_slider_mairon' );

/**
 * Register our sidebars and widgetized areas.
 *
 */
function sidebar_right_widgets_init() {

    register_sidebar( array(
        'name'          => 'Home right sidebar',
        'id'            => 'home_right_1',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'sidebar_right_widgets_init' );

include_once ('woocommerce_hook.php');
/**
 * adminclass
 */
include_once ('lib/admin/class-usage-demo.php');


?>