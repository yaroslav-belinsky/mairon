<?php  $options = get_option('theme_options_panel');  ?>
<div id="sidebar" class="col-sm-3 col-sm-pull-9 col-xs-12">
  <div class="menu-container">
    <h3 class="catalog-sidebar-title">Каталог товаров</h3>
    <?php echo do_shortcode('[product_cat_query_mairon exclude="6, 10"]');?>
  </div>
<div class="row">
	<div class="col-md-12">
		<div class="partners-container">
      <div class="col-md-12">
         <h3 class="partners-title">Партнеры</h3>
      </div>
<?php foreach ($options['re_partners'] as $key => $social) { ?>
          <?php if ($social['re_partners_active']) :?>
             <a href="<?php echo $social['re_partners_link']; ?>" class="col-md-6 partners-item"><img src="<?php echo $social['re_partners_img']['src']; ?>" class="img-responsive" alt="Responsive image"></a>
          <?php  endif; ?>
       <?php  } ?>
   		</div>
   </div>
</div>
<div class="row actions-container">
	<div class="col-md-12">
<?php foreach ($options['re_actions'] as $key => $social) { ?>
          <?php if ($social['re_actions_active']) :?>
             <a href="<?php echo $social['re_actions_link']; ?>" class="col-md-12 actions-item"><img src="<?php echo $social['re_actions_img']['src']; ?>" class="img-responsive" alt="Responsive image"></a>
          <?php  endif; ?>
       <?php  } ?>
     </div>
</div>       
   <?php if ( is_active_sidebar( 'home_right_1' ) ) : ?>
	<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
		<?php dynamic_sidebar( 'home_right_1' ); ?>
	</div><!-- #primary-sidebar -->
<?php endif; ?>
</div>