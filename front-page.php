<?php get_header(); ?>

		<div class="col-sm-9 col-sm-push-3 col-xs-12 page-content-container">
			<?php while (have_posts()) : the_post(); ?>
                         <div class="content">
                            <?php the_content(); ?>
                        </div>
           <?php endwhile; ?>
        </div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>