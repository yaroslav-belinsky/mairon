jQuery(document).ready(function($) {
    var thumbnails = 'a:has(img)[href$=".bmp"],a:has(img)[href$=".gif"],a:has(img)[href$=".jpg"],a:has(img)[href$=".jpeg"],a:has(img)[href$=".png"],a:has(img)[href$=".BMP"],a:has(img)[href$=".GIF"],a:has(img)[href$=".JPG"],a:has(img)[href$=".JPEG"],a:has(img)[href$=".PNG"]';
    $(thumbnails).addClass("fancybox").attr("rel", "group");
    $(".fancybox").fancybox({

    });
    $('.posts-slider').owlCarousel({
        navigation : false, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true,
      items:1,
      pagination: true,
      scrollPerPage:true
    });
    $('.sidebar-menu > li.parent-menu-item > a').click(function(event) {
      $(this).parent('.parent-menu-item').children('.sub-menu').slideToggle('400');
      $(this).parent('.parent-menu-item').toggleClass('active');
      return false;
    });
    var contactsOnMapHeight = $('.contacts-on-map').height() - 30;
    $('.contacts-on-map-center').height(contactsOnMapHeight);
    $('.cart-table > .row').each(function(index, el) {
       
      $(el).find('.product-info > .row > div').first().css('min-height', $(el).find('.product-thumbnail').outerHeight()+'px');
    
    });
    
    
});